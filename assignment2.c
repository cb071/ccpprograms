#include<stdio.h>
#include<math.h>
int main()
{
	float a,b,c,d,root1,root2;
	printf("enter value of a, b and c :");
	scanf("%f%f%f",&a,&b,c);
	d=b*b-4*a*c;
	if(d==0)
	{
		root1=(-b)/(2*a);
		root2=root1;
		printf("roots are real & equal");
	}
	else if(d>0)
	{
		root1=-(b+sqrt(d)/(2*a);
		root2=-(b-sqrt(d)/(2*a);
		printf("roots are real & distinct");
	}
	else
	{
		root1=(-b)/(2*a);
		root2=sqrt(-d)/(2*a);
		printf("roots are imaginary");
	}
	
	printf("root 1 = %f",root1);
	printf("root 2 = %f",root2);
	return 0;